package com.epcc.pcsecurite.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Table(name = "badgemodel",schema = "test_sch")
@Table(name = "badgemodel")
public class BadgeModel{

	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "UID", nullable = false)
	private String uid;
	@Column(name="fdbadge")
	private String fdbadge;
	
	
	/**
	 * 
	 */
	public BadgeModel() {
		super();	
	}
	/**
	 * @param uid
	 * @param numBadge
	 */
	public BadgeModel(String uid, String numBadge) {
		super();
		this.uid = uid;
		this.fdbadge = numBadge;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getFdbadge() {
		return fdbadge;
	}
	public void setFdbadge(String numBadge) {
		this.fdbadge = numBadge;
	}
	
}
