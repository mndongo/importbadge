package com.epcc.pcsecurite.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Table(name = "ldapexport",schema = "test_sch")
@Table(name = "ldapexport")
public class LdapExport {
		@Id
		@Column(name = "UID", nullable = false)
		private String uid ;
		@Column(name="cn")
		private String cn ;
		@Column(name="givenname")
		private String givenname ;
		@Column(name="title")
		private String title ;
		@Column(name="supannentiteaffectation")
		private String supannentiteaffectation ;
		@Column(name="supannentiteaffectationprincipale")
		private String supannentiteaffectationprincipale ;
		@Column(name="telephonenumber")
		private String telephonenumber ;
		@Column(name="mail")
		private String mail ;
		@Column(name="sn")
		private String sn ;
		@Column(name="createtimestamp")
		private String createtimestamp ;
		@Column(name="displayname")
		private String displayname ;
		@Column(name="edupersonaffiliation")
		private String edupersonaffiliation ;
		@Column(name="edupersonprimaryaffiliation")
		private String edupersonprimaryaffiliation ;
		@Column(name="edupersonprincipalname")
		private String edupersonprincipalname ;
		@Column(name="employeenumber")
		private String employeenumber ;
		@Column(name="fdbadge")
		private String fdbadge ;
		@Column(name="fdcontractenddate")
		private String fdcontractenddate ;
		@Column(name="fdcontractstartdate")
		private String fdcontractstartdate ;
		@Column(name="l")
		private String l ;
		@Column(name="mobile")
		private String mobile ;
		@Column(name="modifytimestamp")
		private String modifytimestamp ;
		@Column(name="o")
		private String o ;
		@Column(name="objectclass")
		private String objectclass ;
		@Column(name="ou")
		private String ou ;
		@Column(name="postaladdress")
		private String postaladdress ;
		@Column(name="personaltitle")
		private String personaltitle ;
		@Column(name="roomnumber")
		private String roomnumber ;
		@Column(name="supannetablissement")
		private String supannetablissement ;
		@Column(name="supannmailperso")
		private String supannmailperso ;
		@Column(name="supannetuinscription")
		private String supannetuinscription ;
		@Column(name="supannrefid")
		private String supannrefid ;
		@Column(name="statusdate")
		private String statusdate ;
		@Column(name="recordtype")
		private String recordtype ;
		@Column(name="usergroup")
		private String usergroup ;
		@Column(name="categorytype")
		private String categorytype ;
		@Column(name="statisticcategory")
		private String statisticcategory ;
		@Column(name="usergroupdesc")
		private String usergroupdesc ;
		@Column(name="supannetutypediplome")
		private String supannetutypediplome ;
		@Column(name="compteenddate")
		private String compteenddate;
		
		public String getCn() {
			return cn;
		}
		public void setCn(String cn) {
			this.cn = cn;
		}
		public String getGivenname() {
			return givenname;
		}
		public void setGivenname(String givenname) {
			this.givenname = givenname;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getSupannentiteaffectation() {
			return supannentiteaffectation;
		}
		public void setSupannentiteaffectation(String supannentiteaffectation) {
			this.supannentiteaffectation = supannentiteaffectation;
		}
		public String getSupannentiteaffectationprincipale() {
			return supannentiteaffectationprincipale;
		}
		public void setSupannentiteaffectationprincipale(String supannentiteaffectationprincipale) {
			this.supannentiteaffectationprincipale = supannentiteaffectationprincipale;
		}
		public String getTelephonenumber() {
			return telephonenumber;
		}
		public void setTelephonenumber(String telephonenumber) {
			this.telephonenumber = telephonenumber;
		}
		public String getUid() {
			return uid;
		}
		public void setUid(String uid) {
			this.uid = uid;
		}
		public String getMail() {
			return mail;
		}
		public void setMail(String mail) {
			this.mail = mail;
		}
		public String getSn() {
			return sn;
		}
		public void setSn(String sn) {
			this.sn = sn;
		}
		public String getCreatetimestamp() {
			return createtimestamp;
		}
		public void setCreatetimestamp(String createtimestamp) {
			this.createtimestamp = createtimestamp;
		}
		public String getDisplayname() {
			return displayname;
		}
		public void setDisplayname(String displayname) {
			this.displayname = displayname;
		}
		public String getEdupersonaffiliation() {
			return edupersonaffiliation;
		}
		public void setEdupersonaffiliation(String edupersonaffiliation) {
			this.edupersonaffiliation = edupersonaffiliation;
		}
		public String getEdupersonprimaryaffiliation() {
			return edupersonprimaryaffiliation;
		}
		public void setEdupersonprimaryaffiliation(String edupersonprimaryaffiliation) {
			this.edupersonprimaryaffiliation = edupersonprimaryaffiliation;
		}
		public String getEdupersonprincipalname() {
			return edupersonprincipalname;
		}
		public void setEdupersonprincipalname(String edupersonprincipalname) {
			this.edupersonprincipalname = edupersonprincipalname;
		}
		public String getEmployeenumber() {
			return employeenumber;
		}
		public void setEmployeenumber(String employeenumber) {
			this.employeenumber = employeenumber;
		}
		public String getFdbadge() {
			return fdbadge;
		}
		public void setFdbadge(String fdbadge) {
			this.fdbadge = fdbadge;
		}
		public String getFdcontractenddate() {
			return fdcontractenddate;
		}
		public void setFdcontractenddate(String fdcontractenddate) {
			this.fdcontractenddate = fdcontractenddate;
		}
		public String getFdcontractstartdate() {
			return fdcontractstartdate;
		}
		public void setFdcontractstartdate(String fdcontractstartdate) {
			this.fdcontractstartdate = fdcontractstartdate;
		}
		public String getL() {
			return l;
		}
		public void setL(String l) {
			this.l = l;
		}
		public String getMobile() {
			return mobile;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public String getModifytimestamp() {
			return modifytimestamp;
		}
		public void setModifytimestamp(String modifytimestamp) {
			this.modifytimestamp = modifytimestamp;
		}
		public String getO() {
			return o;
		}
		public void setO(String o) {
			this.o = o;
		}
		public String getObjectclass() {
			return objectclass;
		}
		public void setObjectclass(String objectclass) {
			this.objectclass = objectclass;
		}
		public String getOu() {
			return ou;
		}
		public void setOu(String ou) {
			this.ou = ou;
		}
		public String getPersonaltitle() {
			return personaltitle;
		}
		public void setPersonaltitle(String personaltitle) {
			this.personaltitle = personaltitle;
		}
		public String getPostaladdress() {
			return postaladdress;
		}
		public void setPostaladdress(String postaladdress) {
			this.postaladdress = postaladdress;
		}
		public String getRoomnumber() {
			return roomnumber;
		}
		public void setRoomnumber(String roomnumber) {
			this.roomnumber = roomnumber;
		}
		public String getSupannetablissement() {
			return supannetablissement;
		}
		public void setSupannetablissement(String supannetablissement) {
			this.supannetablissement = supannetablissement;
		}
		public String getSupannmailperso() {
			return supannmailperso;
		}
		public void setSupannmailperso(String supannmailperso) {
			this.supannmailperso = supannmailperso;
		}
		public String getSupannetuinscription() {
			return supannetuinscription;
		}
		public void setSupannetuinscription(String supannetuinscription) {
			this.supannetuinscription = supannetuinscription;
		}
		public String getSupannrefid() {
			return supannrefid;
		}
		public void setSupannrefid(String supannrefid) {
			this.supannrefid = supannrefid;
		}
		public String getStatusdate() {
			return statusdate;
		}
		public void setStatusdate(String statusdate) {
			this.statusdate = statusdate;
		}
		public String getRecordtype() {
			return recordtype;
		}
		public void setRecordtype(String recordtype) {
			this.recordtype = recordtype;
		}
		public String getUsergroup() {
			return usergroup;
		}
		public void setUsergroup(String usergroup) {
			this.usergroup = usergroup;
		}
		public String getCategorytype() {
			return categorytype;
		}
		public void setCategorytype(String categorytype) {
			this.categorytype = categorytype;
		}
		public String getStatisticcategory() {
			return statisticcategory;
		}
		public void setStatisticcategory(String statisticcategory) {
			this.statisticcategory = statisticcategory;
		}
		public String getUsergroupdesc() {
			return usergroupdesc;
		}
		public void setUsergroupdesc(String usergroupdesc) {
			this.usergroupdesc = usergroupdesc;
		}
		public String getSupannetutypediplome() {
			return supannetutypediplome;
		}
		public void setSupannetutypediplome(String supannetutypediplome) {
			this.supannetutypediplome = supannetutypediplome;
		}
		public String getCompteenddate() {
			return compteenddate;
		}
		public void setCompteenddate(String compteenddate) {
			this.compteenddate = compteenddate;
		}
}
