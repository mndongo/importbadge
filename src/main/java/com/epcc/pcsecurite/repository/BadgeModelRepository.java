package com.epcc.pcsecurite.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epcc.pcsecurite.model.BadgeModel;
import com.epcc.pcsecurite.model.LdapExport;
public interface BadgeModelRepository extends JpaRepository<BadgeModel, Long>{
}
