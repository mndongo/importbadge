package com.epcc.pcsecurite.repository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hibernate.query.criteria.internal.predicate.IsEmptyPredicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epcc.pcsecurite.model.BadgeModel;
import com.epcc.pcsecurite.model.LdapExport;


@Service
public class ImportBadgeService {
	Logger logger = LoggerFactory.getLogger(ImportBadgeService.class);
	@Autowired
	ImportBadgeRepository badgeRepository;
	@Autowired
	BadgeModelRepository badgeModelRepository;
	
	public Map<String, String> getAllTutorials() {
		logger.info("get all user ");
		List<LdapExport> allUser= (List<LdapExport>) badgeRepository.findAll();
		Map<String, String> mAllUserWithBadgeFromDB = new HashMap<String, String>();
		//logger.info("size l [0]= "+allUser.get(1).getUid());
		mAllUserWithBadgeFromDB =  allUser.stream().collect(HashMap<String, String>::new, 
				                           (m, c) -> m.put(c.getUid(), c.getFdbadge()),
				                           (m, u) -> {});
		logger.info("size of map = "+mAllUserWithBadgeFromDB.size());
		
		return mAllUserWithBadgeFromDB;
	  }

	/**
	 * parse du fichier csv : élimination des lignes invalides
	 * @param mapforAllUserUidAndBadge
	 */
	public void getBadgeFromCsv(Map<String, String> mapforAllUserUidAndBadge) {
		logger.info("+++ Get Badge From Csv +++++ ");
		
		String fileName = "/opt/echanges/PourDSI/export_ppp.csv";
		
		Pattern pattern = Pattern.compile(",");
		List<BadgeModel> lbadgeUidFromCsv = new ArrayList<BadgeModel>();
		Map<String, String> uidBageFromCsv = new HashMap<String, String>();
		// traitement des lignes non valides
		try (Stream<String> lines = Files.lines(Paths.get(fileName))) {
			
			// creation d'un objet   BadgeModel à partir du fichier csv
			List<BadgeModel> numBadgeAndIud = lines.skip(1).filter(line -> !line.startsWith(",") && !line.endsWith(",")).map(line -> {
				logger.info("line = " +line +" end = "+line.endsWith(","));
		    String[] arr = pattern.split(line);
		    return new BadgeModel(arr[0], arr[1]);
		  }).collect(Collectors.toList());
		  lbadgeUidFromCsv= numBadgeAndIud;
		  // transformation de la liste des badges en une map
		  uidBageFromCsv = transformListToMap(lbadgeUidFromCsv);
		  logger.info("++ size lmodel = "+lbadgeUidFromCsv.size());
		  // test pour comparer les numero de badge et les uid pour construire le fichier csv
		  getUserNotHavingBadgeFromCsv(uidBageFromCsv,mapforAllUserUidAndBadge);
		  
		  
		}catch (FileNotFoundException e) {
			logger.error("error file not found ");
		}catch (Exception e) {
			logger.error("error : "+e.getMessage() +" "+e.getCause());
		}
	}
	
	// compare les utilsateurs qui ont un badge dans le ficier csv et la base de données
	private void getUserNotHavingBadgeFromCsv(Map<String, String> uidBageFromCsv,
			Map<String, String> mapforAllUserUidAndBadge) {
		List<String> lkeyToRemove = new ArrayList<String>();
		logger.info("taille dans la base de données = "+mapforAllUserUidAndBadge.size());
		logger.info("taille dans le csv initial= "+uidBageFromCsv.size());
		for (Map.Entry<String,String> entryFromCsv : uidBageFromCsv.entrySet())  {		 
			for (Map.Entry<String,String> entryFromDb : mapforAllUserUidAndBadge.entrySet())  {
				if(entryFromCsv.getKey().equals(entryFromDb.getKey()) 
						&& (entryFromCsv.getValue().equals(entryFromDb.getValue()))){
					logger.info("+++ est present dans la base et dans le fichier "+entryFromCsv.getKey());
					lkeyToRemove.add(entryFromCsv.getKey());
				}
			}
		}
		
		for(int i=0;i<lkeyToRemove.size();i++) {
			uidBageFromCsv.remove(lkeyToRemove.get(i));
		}
		
		logger.info("taille dans le csv final= "+uidBageFromCsv.size());
		// crée le fichier cdv et inserre dans la base de donnée
		createFileAndSaveBadge(uidBageFromCsv);
	}

	/**
	 * crée le fichier csv
	 * sauvegarde les numero de badge et uid dans la base
	 * @param uidBageFromCsv
	 */
	private void createFileAndSaveBadge(Map<String, String> uidBageFromCsv) {
		List<BadgeModel>  lbadge = new ArrayList<BadgeModel>();
		Iterator it = uidBageFromCsv.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry badgeNumero = (Map.Entry) it.next();
			logger.info("badgeuid = "+String.valueOf(badgeNumero.getKey()));
			logger.info("badgenumero = "+badgeNumero.getValue().toString());
			BadgeModel badgeModel = new BadgeModel(String.valueOf(badgeNumero.getKey()), badgeNumero.getValue().toString());
			lbadge.add(badgeModel);
			badgeModelRepository.save(badgeModel);
		}
		logger.info(" createFileAndSaveBadge : size de ma nouvelle liste = "+lbadge.size());
		// sauvegarde  dans la base
	//	badgeModelRepository.saveAll(lbadge);
		logger.info("finish to save numero badge");
		
	}

	// transforme la liste en une map
	private Map<String, String> transformListToMap(List<BadgeModel> lbadgeUidFromCsv) {
		logger.info("start transformListToMap ");
		Map<String, String> mUidBadgeFromCsv = new HashMap<String, String>();
		mUidBadgeFromCsv = lbadgeUidFromCsv.stream().collect(HashMap<String, String>::new, 
                (m, c) -> m.put(c.getUid(), c.getFdbadge()),
                (m, u) -> {});
			logger.info("size of map from csv = "+mUidBadgeFromCsv.size());
			for (Map.Entry<String,String> entry : mUidBadgeFromCsv.entrySet())  {
			logger.info("from csv Key   = " + entry.getKey() + 
			", Value = " + entry.getValue()); 			

		}
		return mUidBadgeFromCsv;
	}
}
