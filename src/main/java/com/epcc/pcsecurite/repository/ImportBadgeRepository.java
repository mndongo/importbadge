package com.epcc.pcsecurite.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epcc.pcsecurite.model.LdapExport;
public interface ImportBadgeRepository extends JpaRepository<LdapExport, Long>{
}
