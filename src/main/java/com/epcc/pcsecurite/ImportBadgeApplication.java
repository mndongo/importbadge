package com.epcc.pcsecurite;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.epcc.pcsecurite.repository.ImportBadgeRepository;
import com.epcc.pcsecurite.repository.ImportBadgeService;

@SpringBootApplication
public class ImportBadgeApplication implements CommandLineRunner{
	Logger logger = LoggerFactory.getLogger(ImportBadgeApplication.class);
	@Autowired
	ImportBadgeRepository repo;
	@Autowired
	ImportBadgeService service;
	public static void main(String[] args) {
		SpringApplication.run(ImportBadgeApplication.class, args);
	}
	
	@Override
    public void run(String... arg0)throws Exception{
		logger.info("*** start application import badge");
		logger.info("call service import ");
		Map<String, String> result = service.getAllTutorials();
		service.getBadgeFromCsv(result);
		
    };

}
